class Program:
    __program_set = []
    __language_set = {}

    def __init__(self, filename=None):
        if not filename:
            return

        with open(filename, "r") as f:
            for line in f.readlines():
                line = line.strip()
                if line:
                    instruction = [item.strip(" ,") for item in line.split()]
                    instruction.insert(0, len(instruction) - 1)
                    self.__program_set.append(instruction)

    def get_program(self):
        return self.__program_set

    def set_program_line(self, line):
        self.__program_set.append(line)
