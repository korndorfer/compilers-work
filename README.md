# Compilers Work

Write an assembler and a simulator for a specific language defined.

The language contains the list of words and binary values below:

*  add = 000
*  sub = 001
*  wr  = 010
*  rd  = 011
*  inc = 100
*  dec = 101

The architecture is a 8 bit machine with a single register 'A' and a memory with addresses starting in 00h to 0Fh.

The assembler will translate the words in values and write to file.

The simulator will read the file and interpret the code.


How to run
--------

python translator.py -> will create the file data/result.bin

python simulator.py will read data/result.bin and show the results on screen
