from program import Program


class Computer:
    __registers = {}
    __opcodes = {}
    __memory = {}
    __databits = None
    __bytecodes = {}
    __program = Program()
    __default_value = None

    def __init__(self, databits=8, registers=None, memory=None,
                 opcodes=None):
        self.__databits = databits
        self.__opcodebits = 3
        self.__default_value = "0" * databits
        self.set_registers(registers)
        self.set_memory(memory)
        self.set_opcodes(opcodes)
        self.set_opcodebits()
        self.show_state("Initial")

    def _to_binary(self, value):
        binary = bin(value).split("b")[1]
        return "0" * (8 - len(binary)) + binary

    def set_opcodebits(self):
        self.__opcodebits = len(self.__opcodes.keys()[0])

    def set_registers(self, registers):
        self.__registers = {value[0]: {"opcode": key,
                                       "value": self.__default_value
                                       } for (key, value) in registers.items()}

    def set_memory(self, memory):
        self.__memory = {value[0]: {"opcode": key,
                                    "value": self.__default_value
                                    } for (key, value) in memory.items()}

    def set_opcodes(self, opcodes):
        self.__opcodes = {value[-1]: {"parameter_count": value[0],
                                      "parameter_type": value[1:-1],
                                      "opcode": key
                                      } for (key, value) in opcodes.items()}

    def load_bytecodes(self, bytecodes):
        self.__bytecodes = bytecodes
        self.__latest_position = len(bytecodes)
        self.show_state("After load program")

    def run_program(self):
        mc = 0
        bytecode = None
        while mc < self.__latest_position:
            if not bytecode:
                st = mc
                mc = mc+self.__opcodebits
                bytecode = self.__bytecodes[st:mc]
                if bytecode not in self.__opcodes.keys():
                    print("ERROR: bytecode {} not found.".format(bytecode))
                    bytecode = ""
                    continue
                opcode = self.__opcodes[bytecode]
                parameters = []
                cont = 0

                while cont < int(opcode["parameter_count"]):
                    st = mc
                    mc = mc + self.__databits
                    parameters.append(self.__bytecodes[st:mc])
                    cont = cont + 1

                if opcode["parameter_type"][0] == 'r' and\
                   parameters[0] in self.__registers:
                    res = self.__registers[parameters[0]]
                elif opcode["parameter_type"][0] == 'm' and\
                     parameters[0] in self.__memory:
                    res = self.__memory[parameters[0]]

                if opcode["opcode"] in "add":
                    if opcode["parameter_type"][1] == 'n':
                        add = int(parameters[1], 2)
                    elif opcode["parameter_type"][1] == 'm':
                        add = int(self.__memory[parameters[1]]["value"], 2)
                    res["value"] = self._to_binary(int(res["value"], 2) + add)
                    self.show_state("After 'add' operation")
                elif opcode["opcode"] in "sub":
                    if opcode["parameter_type"][1] == 'n':
                        sub = int(parameters[1], 2)
                    elif opcode["parameter_type"][1] == 'm':
                        sub = int(self.__memory[parameters[1]]["value"], 2)
                    res["value"] = self._to_binary(int(res["value"], 2) - sub)
                    self.show_state("After 'sub' operation")
                elif opcode["opcode"] in "wr":
                    if opcode["parameter_type"][1] == 'r':
                        wr = self.__registers[parameters[1]]
                    elif opcode["parameter_type"][1] == 'm':
                        wr = self.__memory[parameters[1]]
                    wr["value"] = res["value"]
                    self.show_state("After 'wr' operation")
                elif opcode["opcode"] in "rd":
                    if opcode["parameter_type"][1] == 'r':
                        rd = self.__registers[parameters[1]]
                    elif opcode["parameter_type"][1] == 'm':
                        rd = self.__memory[parameters[1]]
                    res["value"] = rd["value"]
                    self.show_state("After 'rd' operation")
                elif opcode["opcode"] in "inc":
                    res["value"] = self._to_binary(int(res["value"], 2) + 1)
                    self.show_state("After 'inc' operation")
                elif opcode["opcode"] in "dec":
                    res["value"] = self._to_binary(int(res["value"], 2) - 1)
                    self.show_state("After 'dec' operation")

                bytecode = ""

    def show_state(self, title):
        size = 0
        for count in (len(self.__registers),
                      len(self.__bytecodes)/20, len(self.__memory)):
            if count > size:
                size = count

        result = []
        for i in range(size):
            result.append(["", "", ""])

        idx = 0
        for key, value in self.__registers.items():
            result[idx][0] = "{0}:{1}".format(value["opcode"], value["value"])
            idx = idx + 1

        idx = 0
        for key, value in sorted(self.__memory.items()):
            result[idx][1] = "{0}:{1}".format(key, value["value"])
            idx = idx + 1

        idx = 0
        pos = 0
        aux = ""
        for char in self.__bytecodes:
            aux = aux + char
            pos = pos + 1
            if pos >= 20:
                result[idx][2] = "{}".format(aux)
                aux = ""
                pos = 0
                idx = idx + 1
        result[idx][2] = "{}".format(aux)

        print("{}".format("#" * 80))
        print("{0} bits computer simulator. {1} state:".format(
              self.__databits, title))
        print("".format(""))
        print("{0:<11}{1:^22}{2:^20}".format("REGISTERS", "DATA MEMORY",
                                             "PROGRAM MEMORY"))
        for reg, mem, pro in result:
            print("{0:<11}{1:^22}{2:<20}".format(reg, mem, pro))
        print("{}".format("#" * 80))
