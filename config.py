class Config:
    __config_set = {}

    def __init__(self, filename=None):
        aux = None
        with open(filename, "r") as f:
            for line in f.readlines():
                line = line.strip()
                if line.startswith("[") and line.endswith("]"):
                    aux = line[1:-1]
                    self.__config_set[aux] = {}
                else:
                    if aux is not None and line:
                        key, parameters = line.split("=")
                        self.__config_set[aux][key] = parameters.split(",")

    def get_language(self):
        if "language" not in self.__config_set:
            return {}
        return self.__config_set["language"]

    def get_register(self):
        if "register" not in self.__config_set:
            return {}
        return self.__config_set["register"] or None

    def get_memory(self):
        if "memory" not in self.__config_set:
            return {}
        return self.__config_set["memory"] or None

    def get_databits(self):
        if "data" not in self.__config_set:
            if "bits" not in self.__config_set["data"]:
                return 8
        return int(self.__config_set["data"]["bits"][0]) or None
