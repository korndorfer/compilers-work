# -*- coding: utf-8 -*-
from config import Config
from computer import Computer

if __name__ == "__main__":
    conf_file = "data/set.dat"
    resu_file = "data/result.bin"

    conf = Config(conf_file)
    computer = Computer(conf.get_databits(), conf.get_register(),
                        conf.get_memory(), conf.get_language())

    with open(resu_file) as f:
        computer.load_bytecodes(f.readlines()[0])

    computer.run_program()