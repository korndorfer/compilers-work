# -*- coding: utf-8 -*-
from config import Config
from program import Program


def to_binary(value):
    binary = bin(value).split("b")[1]
    return "0" * (8 - len(binary)) + binary


if __name__ == "__main__":
    conf_file = "data/set.dat"
    prog_file = "data/data.dat"
    resu_file = "data/result.bin"
    buffer = ""

    conf = Config(conf_file)
    language = conf.get_language()
    registers = conf.get_register()

    prog = Program(prog_file)

    for line in prog.get_program():
        number_of_parameters = line[0]
        instruction = line[1:]
        for parameter in instruction:
            if parameter in language.keys():
                if number_of_parameters == int(language[parameter][0]):
                    buffer += language[parameter][-1]
                else:
                    error = "number of parameters mismatch."
                    print("Error in instruction {0}: '{1}'".format(
                          error, " ".join(instruction)))
                    break
            elif parameter in registers.keys():
                buffer += registers[parameter][0]
            else:
                buffer += to_binary(int(parameter))

    with open(resu_file, "w") as f:
        f.write(buffer)
